package br.com.alura.loja.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FabricaEntityManager {

    /** fabrica para criação do entity manager e o nome da persistencia que esta dentro do persistence.xml 'loja'**/
    private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory("loja");

    /** cria um entity manager para gerenciar a jpa **/
    public static EntityManager getEntityManager(){
        return factory.createEntityManager();
    }
}
