package br.com.alura.loja.dao;

import br.com.alura.loja.entidades.ItemPedido;
import br.com.alura.loja.entidades.Pedido;

import javax.persistence.EntityManager;

public class ItemPedidoDao {

    private final EntityManager entityManager;

    public ItemPedidoDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void salvarItemPedido(ItemPedido itemPedido){
        entityManager.persist(itemPedido);
    }

    public void atualizaPedido(ItemPedido itemPedido){
        entityManager.merge(itemPedido);
    }

    public void excluir(ItemPedido itemPedido){
        entityManager.remove(itemPedido);
    }
}
