package br.com.alura.loja.dao;

import br.com.alura.loja.entidades.Cliente;

import javax.persistence.EntityManager;

public class ClienteDao {

    private final EntityManager entityManager;

    public ClienteDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void salvarCliente(Cliente cliente){
        entityManager.persist(cliente);
    }

    public void atualizaCliente(Cliente cliente){
        entityManager.merge(cliente);
    }

    public void excluir(Cliente cliente){
        entityManager.remove(cliente);
    }
}
