package br.com.alura.loja.dao;

import br.com.alura.loja.entidades.Pedido;

import javax.persistence.EntityManager;

public class PedidoDao {

    private final EntityManager entityManager;

    public PedidoDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void salvarPedido(Pedido pedido){
        entityManager.persist(pedido);
    }

    public void atualizaPedido(Pedido pedido){
        entityManager.merge(pedido);
    }

    public void excluir(Pedido pedido){
        entityManager.remove(pedido);
    }

    public Pedido buscarPorId(Long id){
      return entityManager.find(Pedido.class, id);    }
}
