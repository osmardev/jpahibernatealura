package br.com.alura.loja.dao;

import br.com.alura.loja.entidades.Produto;

import javax.persistence.EntityManager;
import java.util.List;

public class ProdutoDao {

    private final EntityManager entityManager;

    public ProdutoDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void salvarProduto(Produto produto){
        entityManager.persist(produto);
    }

    public void atualizarProduto(Produto produto){
        entityManager.persist(produto);
    }

    public Produto buscarPorId(Long id){
       return entityManager.find(Produto.class, id);
    }

    public List<Produto> buscarTodos(){
        String jpql = "SELECT p from Produto p";
        return entityManager.createQuery(jpql, Produto.class).getResultList();
    }

    public List<Produto> buscarPorNome(String nome){
        String jpql = "SELECT p from Produto p WHERE p.nome = :nome ";
        return entityManager.createQuery(jpql, Produto.class)
                .setParameter("nome", nome)
                .getResultList();
    }

    public List<Produto> buscarPorPreco(Double precoMaiorIgual,Double precoMenorIgual ){
        String jpql = "SELECT p from Produto p WHERE p.preco >= :preco AND p.preco <= :preco2";
        return entityManager.createQuery(jpql, Produto.class)
                .setParameter("preco", precoMaiorIgual)
                .setParameter("preco2", precoMenorIgual)
                .getResultList();
    }

    public List<Produto> buscaPorDescricaoENome(String descricao, String nome){
        String jqpl = "select p from Produto p where p.descricao = :?1 AND p.nome = :?2";
        return entityManager.createQuery(jqpl, Produto.class)
                .setParameter("1",descricao )
                .setParameter("2",nome )
                .getResultList();
    }

    public Double buscaOPrecoDoProdutoComNome(String nome){
        String jqpl = "select p.preco from Produto p where p.nome = :nome";
        return entityManager.createQuery(jqpl, Double.class)
                .setParameter("nome", nome)
                .getSingleResult();
    }


}
