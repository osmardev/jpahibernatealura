package br.com.alura.loja.dao;

import br.com.alura.loja.entidades.Categoria;

import javax.persistence.EntityManager;

public class CategoriaDao {

    private final EntityManager entityManager;

    public CategoriaDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void salvarCategoria(Categoria categoria){
        entityManager.persist(categoria);
    }

    public void atualizaCategoria(Categoria categoria){
        entityManager.merge(categoria);
    }

    public void excluir(Categoria categoria){
        entityManager.remove(categoria);
    }
}
