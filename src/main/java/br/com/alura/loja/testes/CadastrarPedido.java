package br.com.alura.loja.testes;

import br.com.alura.loja.dao.*;
import br.com.alura.loja.entidades.*;
import br.com.alura.loja.util.FabricaEntityManager;

import javax.persistence.EntityManager;

public class CadastrarPedido {

    public static void main(String[] args) {
        EntityManager entityManager = FabricaEntityManager.getEntityManager();
        Cliente cliente = new Cliente("0934*******", "Osmar Reche Junior");

        Categoria categoria = new Categoria("celulares");
        Produto produto = new Produto("Redmi note 9", "Celular top", 5000.00, categoria);
        Produto produto2 = new Produto("Redmi note 10", "Celular top", 7000.00, categoria);

        ClienteDao clienteDao = new ClienteDao(entityManager);
        CategoriaDao categoriaDao = new CategoriaDao(entityManager);
        ProdutoDao produtoDao = new ProdutoDao(entityManager);
        PedidoDao pedidoDao = new PedidoDao(entityManager);
        ItemPedidoDao itemPedidoDao = new ItemPedidoDao(entityManager);

        Pedido pedido = new Pedido(cliente);
        ItemPedido itemPedido = new ItemPedido(10, produto);
        ItemPedido itemPedido2 = new ItemPedido(12, produto2);

        entityManager.getTransaction().begin();
        clienteDao.salvarCliente(cliente);
        categoriaDao.salvarCategoria(categoria);
        produtoDao.salvarProduto(produto);
        produtoDao.salvarProduto(produto2);
        itemPedidoDao.salvarItemPedido(itemPedido);
        itemPedidoDao.salvarItemPedido(itemPedido2);
        pedido.adicionarItemPedido(itemPedido);
        pedido.adicionarItemPedido(itemPedido2);
        pedidoDao.salvarPedido(pedido);
        System.out.println("Realizando uma venda");

        entityManager.getTransaction().commit();

        Pedido pedidoFeito = pedidoDao.buscarPorId(1L);
        System.out.println("Pedido Nº " + pedidoFeito.getId());
        System.out.println("Nome do cliente: " + pedidoFeito.getCliente().getNome());
        System.out.println("Data da venda: " + pedidoFeito.getData());
        pedidoFeito.getItens().forEach(it -> System.out.println("Item pedido n° " + it.getId() + " " + it.toString()));
        System.out.println("Valor total: " + pedidoFeito.getValorTotal());

    }
}
