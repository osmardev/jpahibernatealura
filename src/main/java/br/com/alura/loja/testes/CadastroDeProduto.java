package br.com.alura.loja.testes;

import br.com.alura.loja.dao.CategoriaDao;
import br.com.alura.loja.dao.ProdutoDao;
import br.com.alura.loja.entidades.Categoria;
import br.com.alura.loja.entidades.Produto;
import br.com.alura.loja.util.FabricaEntityManager;

import javax.persistence.EntityManager;
import java.util.List;

public class CadastroDeProduto {

    public static void main(String[] args) {

       executar();
        Long id = 1l;

        EntityManager em = FabricaEntityManager.getEntityManager();
        ProdutoDao produtoDao = new ProdutoDao(em);
        Produto produtoBuscado = produtoDao.buscarPorId(id);
        System.out.println(produtoBuscado.getNome() +  " " + produtoBuscado.getDataCadastro());
        System.out.println(produtoBuscado.getCategoria().getNome() +  " " + produtoBuscado.getCategoria().getId());

        List<Produto> listaProduto = produtoDao.buscarTodos();

        List<Produto> listaProdutoNome = produtoDao.buscarPorNome("Xiaomi Redmi");
        List<Produto> listaProdutoPorPreco = produtoDao.buscarPorPreco(2500.00, 4500.00);


        listaProduto.forEach(p -> System.out.println("Busca todos : " + p.getNome()));

        listaProdutoNome.forEach(p -> System.out.println("Busca por nome: " + p.getNome()));


        listaProdutoPorPreco.forEach(p -> System.out.println("Busca por preco: " + p.getNome()));

        Double preco = produtoDao.buscaOPrecoDoProdutoComNome("Xiaomi Redmi");

        System.out.println("Preco do produto " + preco);


    }



    public static void executar(){
        Produto celular = new Produto();
        Produto celular2 = new Produto();
        Produto celular3 = new Produto();
        Produto celular4 = new Produto();

        celular.setNome("Xiaomi Redmi");
        celular.setDescricao("Muito Legal");
        celular.setPreco(800.00);

        celular2.setNome("Galaxy S20");
        celular2.setDescricao("top de linha samsung");
        celular2.setPreco(2500.00);

        celular3.setNome("Galaxy S20+");
        celular3.setDescricao("top de linha samsung");
        celular3.setPreco(4500.00);


        celular4.setNome("Galaxy S20 Ultra");
        celular4.setDescricao("top de linha samsung");
        celular4.setPreco(6500.00);


        Categoria categoria = new Categoria("celulares");

        EntityManager em = FabricaEntityManager.getEntityManager();
        ProdutoDao produtoDao = new ProdutoDao(em);
        CategoriaDao categoriaDao = new CategoriaDao(em);
        /** pega a transação e inicia **/
        em.getTransaction().begin();
        categoriaDao.salvarCategoria(categoria);
        celular.setCategoria(categoria);
        celular2.setCategoria(categoria);
        celular3.setCategoria(categoria);
        celular4.setCategoria(categoria);
        /** realiza um insert e salva o celular na base de dados **/
        produtoDao.salvarProduto(celular);
        produtoDao.salvarProduto(celular2);
        produtoDao.salvarProduto(celular3);
        produtoDao.salvarProduto(celular4);
        /** pega a transação e commita **/
        Produto produto = em.find(Produto.class, 1l );
        System.out.println(produto.getCategoria().getNome());
        System.out.println(produto.getCategoria().getId());

        em.getTransaction().commit();
        /** fecha o entity manager **/
        em.close();
    }

    public static void exercutar2(){
        Categoria categoria = new Categoria("celulares");
        Categoria categoria1 = new Categoria("outros");

        EntityManager em = FabricaEntityManager.getEntityManager();
        CategoriaDao categoriaDao = new CategoriaDao(em);
        em.getTransaction().begin();
        categoriaDao.salvarCategoria(categoria);
        categoria.setNome("informatica");
        em.flush();
        em.clear();
        categoria.setNome("celulares");
        em.flush();
        categoria = em.merge(categoria);
        categoria.setNome("notebook");
        em.flush();
        categoriaDao.atualizaCategoria(categoria);
        categoriaDao.excluir(categoria);
        em.getTransaction().commit();
    }
}
